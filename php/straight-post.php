<?php
include('curl.php');

// Example lead data of Auto Insurance
$apiKey = '';
$leadData = [
    'email'                      => 'hcl' . rand(5555555555, 8888888888) . '@gmail.com',
    'phone_number'               => rand(5555555555, 8888888888),
    'first_name'                 => 'Yesmar',
    'last_name'                  => 'Somar',
    'zip'                        => '01110',
    'city'                       => 'New York',
    'state'                      => 'NY',
    'address'                    => '143 Street',
    'category_id'                => '1',
    'api_key'                    => $apiKey,
    'dob'                        => '02/23/2015',
    'gender'                     => 'M',
    'vehicle_make'               => 'Honda',
    'vehicle_model'              => 'Civic',
    'vehicle_year'               => '2002',
    'vehicle_submodel'           => 'Sedan',
    'ownership'                  => 'Owned',
    'primary_use'                => 'Commute Work',
    'driver_license_state'       => 'NY',
    'driver_licensed_age'        => '16',
    'current_occupation'         => 'Employed',
    'marital_status'             => 'Married',
    'residence_type'             => 'Renting',
    'credit_score'               => '700+',
    'collision_deductible'       => '100',
    'comphrensive_deductible'    => '100',
    'desired_medical_deductible' => '100',
    'coverage_amount'            => '50/100/50',
    'annual_miles'               => '1300',
    'daily_mileage'              => '5',
    'sr22'                       => 'No',
    'education_level'            => 'GED',
    'incidents'                  => 'No',
    'insurance_company'          => 'Geico',
    'test'                       => 1
];

if (empty($leadData['api_key'])) {
    echo 'Invalid API Key';
    exit();
}

$straighPostUrl = 'https://www.highcaliberleads.com/api/v1/leads.json';
$responseData = curlPost($straighPostUrl, $leadData);

echo $responseData;