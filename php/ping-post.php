<?php
include('curl.php');

// Example lead data of Med Supplement
$phoneNumber = rand(5555555555, 8888888888);
$apiKey = '';
$categoryId = 22;
$pingData = [
    'api_key' => $apiKey,
    'category_id' => $categoryId,
    'city' => 'Houston',
    'state' => 'TX',
    'zip' => '77028',
    'dob' => '01/31/1950',
    'age' => '65.00',
    'gender' => 'M',
    'phone_number' => substr($phoneNumber, -4),
    'test' => 1
];

$postData = [
    'api_key' => $apiKey,
    'subid' => '140015',
    'ip_address' => '104.59.248.144',
    'universal_lead_id' => 'ac3ceb5d-7412-1236-7777-1bc7f017ccfc',
    'user_agent' => 'Not Collected',
    'source_url' => 'http://trustedformurlhere.com',
    'tcpa_language' => 'We take your privacy seriously. By saying yes, you agree to share your information with quoteandprotect and up to 4 of its Premier Partners and for them to contact you (including through automated means; e.g. autodialing, text and pre-recorded messaging) via telephone, mobile device (including SMS and MMS) and/or email, even if your telephone number is currently listed on any state, federal or corporate Do Not Call list.',
    'first_name' => 'Mike',
    'last_name' => 'Wood',
    'phone_number' => '3168596944',
    'email' => 'asdf' . substr($phoneNumber, -4) . '@gmail.com',
    'address' => '21, South Park',
    'city' => 'Fort Lauderdale',
    'state' => 'TX',
    'zip' => '78705',
    'height_ft' => '6',
    'height_in' => '7',
    'weight' => '150',
    'medical_condition' => 'Other',
    'tobacco_use' => 'NO',
    'test' => 1
];

if (empty($pingData['api_key'])) {
    echo 'Invalid API Key for Ping request.';
    exit();
}

if (empty($postData['api_key'])) {
    echo 'Invalid API Key for Post request.';
    exit();
}


$pingUrl = 'https://www.highcaliberleads.com/api/v1/leads/ping.json';
$responseData = curlPost($pingUrl, $pingData);
$responsePingData = json_decode($responseData);

if ($responsePingData->status == 1) {
    // proceed with post request
    $postData['lead_id'] = $responsePingData->lead_id;
    $postUrl = 'https://www.highcaliberleads.com/api/v1/leads/post.json';
    $responseData = curlPost($postUrl, $postData);
}

echo $responseData;